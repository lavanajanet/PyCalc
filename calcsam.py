from tkinter import *
import MathAction

text1 = " "
text2 = " "
flagDot = 0
flagDotRepeat = 0
leftNum = 0
rightNum = 0
result = 0
action = " "

def onClickNUM(self):
    global leftNum, rightNum, action, flagDot, result
    result = 0
    buttonClk = self.widget
    if action == " ":
        if flagDot == 1:
            leftNum = float(text1 + buttonClk['text'])
            theDisplay.config(text = str(leftNum))
            flagDot = 0
        else:
            leftNum = int(str(leftNum) + buttonClk['text'])
            theDisplay.config(text = str(leftNum))
    else:
        
        if flagDot == 1:
            rightNum = float(text2 + buttonClk['text'])
            theDisplay.config(text = str(rightNum))
            flagDot = 0
            
        else:
            rightNum = int(str(rightNum) + buttonClk['text'])
            theDisplay.config(text = str(rightNum))
    
def clearScreen():
    global action, leftNum, rightNum
    action = " "
    leftNum = 0
    rightNum = 0

def onClickOPP(self):
    global action, flagDotRepeat, result, leftNum
    flagResult = 0    
    if result != 0:
        leftNum = result
        result = 0
    flagDotRepeat = 0
    buttonClk = self.widget
    action = buttonClk['text']

def onClickEQL(self):
    global leftNum, rightNum, action, result
    print(action,leftNum,rightNum)
    result = MathAction.action(action,leftNum,rightNum)
    print(result)
    theDisplay.config(text = str(result))
    clearScreen()
    

def onClickDOT(self):
    global leftNum, rightNum, action, text1, text2, flagDot, flagDotRepeat
    if flagDotRepeat == 0:
        flagDot = 1
        flagDotRepeat = 1
        buttonClk = self.widget
        if action == " ":
            text1 = str(leftNum) + buttonClk['text']
            theDisplay.config(text = text1)
        else:
            text2 = str(rightNum) + buttonClk['text']
            theDisplay.config(text = text2)
        
    
root = Tk()
theLable = Label(root, text = "Calculater")
theLable.pack()
theDisplay = Label(root, text = "0", anchor = E, width = 30, bg = "white")
theDisplay.pack()
button_ce = Button(root, text = "CE", width = 10)
#button_ce.bind('<Button-1>', onClickNUM)
button_ce.pack(side = LEFT)
    

Bframe1 = Frame(root)
button_7 = Button(Bframe1, text = "7", width = 10)
button_7.bind('<Button-1>', onClickNUM)
button_7.pack(side = LEFT)
button_8 = Button(Bframe1, text = "8", width = 10)
button_8.bind('<Button-1>', onClickNUM)
button_8.pack(side = LEFT)
button_9 = Button(Bframe1, text = "9", width = 10)
button_9.bind('<Button-1>', onClickNUM)
button_9.pack(side = LEFT)
button_div = Button(Bframe1, text = "%", width = 10)
button_div.bind('<Button-1>', onClickOPP)
button_div.pack(side = LEFT)


Bframe2 = Frame(root)
button_4 = Button(Bframe2, text = "4", width = 10)
button_4.bind('<Button-1>', onClickNUM)
button_4.pack(side = LEFT)
button_5 = Button(Bframe2, text = "5", width = 10)
button_5.bind('<Button-1>', onClickNUM)
button_5.pack(side = LEFT)
button_6 = Button(Bframe2, text = "6", width = 10)
button_6.bind('<Button-1>', onClickNUM)
button_6.pack(side = LEFT)
button_mul = Button(Bframe2, text = "*", width = 10)
button_mul.bind('<Button-1>', onClickOPP)
button_mul.pack(side = LEFT)

Bframe3 = Frame(root)
button_1 = Button(Bframe3, text = "1", width = 10)
button_1.bind('<Button-1>', onClickNUM)
button_1.pack(side = LEFT)
button_2 = Button(Bframe3, text = "2", width = 10)
button_2.bind('<Button-1>', onClickNUM)
button_2.pack(side = LEFT)
button_3 = Button(Bframe3, text = "3", width = 10)
button_3.bind('<Button-1>', onClickNUM)
button_3.pack(side = LEFT)
button_sub = Button(Bframe3, text = "-", width = 10)
button_sub.bind('<Button-1>', onClickOPP)
button_sub.pack(side = LEFT)

Bframe4 = Frame(root)
button_0 = Button(Bframe4, text = "0", width = 10)
button_0.bind('<Button-1>', onClickNUM)
button_0.pack(side = LEFT)
button_dot = Button(Bframe4, text = ".", width = 10)
button_dot.bind('<Button-1>', onClickDOT)
button_dot.pack(side = LEFT)
button_add = Button(Bframe4, text = "+", width = 10)
button_add.bind('<Button-1>', onClickOPP)
button_add.pack(side = LEFT)
button_equal = Button(Bframe4, text = "=", width = 10)
button_equal.bind('<Button-1>', onClickEQL)
button_equal.pack(side = LEFT)

Bframe1.pack()
Bframe2.pack()
Bframe3.pack()
Bframe4.pack()
root.mainloop()
