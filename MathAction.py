def action(math, a, b):    
    c = 0
    if math == "+":
        c = round(a + b, 2)
    if math == "-":
        c = round(a - b, 2)
    if math == "*":
        c = round(a * b, 2)
    if math == "%":
        c = round(a / b, 2)
    return c     